# Mes --- Maxwell Equations of Software
# Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of Mes.
#
# Mes is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Mes is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mes.  If not, see <http://www.gnu.org/licenses/>.

../mes/guile/mescc.scm -c -E -I ../mes/mlibc/include -o hex.E ../stage0/Linux\ Bootstrap/hex.c
../mes/guile/mescc.scm -c -o hex.M1 hex.E
../mescc-tools/bin/M1 --LittleEndian --Architecture=1 -f ../mes/stage0/x86.M1 -f ../mes/mlibc/libc-mes+tcc.M1 -f hex.M1 > hex.hex2
../mescc-tools/bin/hex2 --LittleEndian --Architecture=1 --BaseAddress=0x1000000 -f ../mes/stage0/elf32-header.hex2 -f ../mes/mlibc/crt1.hex2 -f ../mes/mlibc/libc-mes+tcc.hex2 -f hex.hex2 -f ../mes/stage0/elf32-footer-single-main.hex2 > hex0
chmod +x hex0
rm hex.E hex.M1 hex.hex2

./hex0 < ../stage0/Linux\ Bootstrap/hex0.hex > hex1
chmod +x hex1
rm hex0

./hex1 < ../stage0/Linux\ Bootstrap/hex0.hex > hex
chmod +x hex
cmp hex1 hex
rm hex1
